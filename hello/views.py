from django.shortcuts import render
from django.shortcuts import redirect
from .models import Event
from .serializers import EventSerializer
import datetime
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, logout, login
from rest_framework import viewsets
from django.shortcuts import get_object_or_404


class EventViewSet(viewsets.ModelViewSet):
    serializer_class = EventSerializer
    queryset = Event.objects.all()


# Create your views here.
def hello(request):

    if '_auth_user_id' not in request.session.keys():
        log = False
    else:
        log = User.objects.get(id=request.session['_auth_user_id'])

    if request.method == 'POST':
        data = request.POST['data']
        if not data:
            data = datetime.datetime.date(datetime.datetime.today())
    else:
        data = datetime.datetime.date(datetime.datetime.today())
    events = Event.objects.filter(date=data)

    groups = [
        {
            'name': "Парки",
            'preset': "islands#greenGlyphIcon",
            'iconGlyph': "tree-deciduous",
            'iconGlyphColor': "green",
            'items': []
         },
        {
            'name': "Театры и музеи",
            'preset': "islands#brownGlyphIcon",
            'iconGlyph': "home",
            'iconGlyphColor': "brown",
            'items': []
        },
        {
            'name': "Концерты",
            'preset': "islands#redGlyphIcon",
            'iconGlyph': "music",
            'iconGlyphColor': "red",
            'items': []
        },
        {
            'name': "Бары",
            'preset': "islands#blueGlyphIcon",
            'iconGlyph': "glass",
            'iconGlyphColor': "blue",
            'items': []
        },
        {
            'name': "Кинотеатры",
            'preset': "islands#blackGlyphIcon",
            'iconGlyph': "film",
            'iconGlyphColor': "black",
            'items': []
        },
    ]

    for event in events:
        for group in groups:
            if event.type == group['iconGlyph']:
                group['items'].append(event)

    return render(request, 'hello/index.html', {'groups': groups, 'data': data, 'log': log})


def log(request):
    if request.method == 'POST':
        user = authenticate(username=request.POST['user'], password=request.POST['pass'])
        if user and user.is_active == True:
            login(request, user)
            return redirect('/')
        else:
            return render(request, 'hello/log.html', {'message': 'Неверный логин или пароль'})
    return render(request, 'hello/log.html')


def sign(request):
    if request.method == 'POST':
        try:
            user = User.objects.create_user(username=request.POST['user'], password=request.POST['pass'])
            user.save()
        except:
            pass
        return redirect('/login')
    return render(request, 'hello/sign.html')


def out(request):
    logout(request)
    return redirect('/')
