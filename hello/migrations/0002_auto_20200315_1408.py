# Generated by Django 3.0.4 on 2020-03-15 11:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hello', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='coordinate',
        ),
        migrations.AddField(
            model_name='event',
            name='coordinate_x',
            field=models.FloatField(default=59.93863),
        ),
        migrations.AddField(
            model_name='event',
            name='coordinate_y',
            field=models.FloatField(default=30.31413),
        ),
    ]
