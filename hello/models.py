from django.db import models
import datetime


# Create your models here.
class Event(models.Model):
    date = models.DateField(default=datetime.datetime.date(datetime.datetime.today()))
    name = models.CharField(max_length=50, default="")
    description = models.TextField(max_length=250, default="")
    coordinate_x = models.FloatField(default=59.9386300)
    coordinate_y = models.FloatField(default=30.3141300)
    image = models.TextField(max_length=250, default="")
    address = models.CharField(max_length=50, default="")
    type = models.CharField(max_length=50, default="")
