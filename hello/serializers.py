from rest_framework import serializers
from .models import Event
import datetime


class EventSerializer(serializers.Serializer):
    date = serializers.DateField(default=datetime.datetime.date(datetime.datetime.today()))
    name = serializers.CharField(max_length=50, default="")
    description = serializers.CharField(max_length=250, default="")
    coordinate_x = serializers.FloatField(default=59.9386300)
    coordinate_y = serializers.FloatField(default=30.3141300)
    image = serializers.CharField(max_length=250, default="")
    address = serializers.CharField(max_length=50, default="")
    type = serializers.CharField(max_length=50, default="")

    def create(self, validated_data):
        return Event.objects.create(**validated_data)
