import pytest
from django.urls import reverse
from django.contrib.auth.models import User
from hello.views import hello, EventViewSet, log, sign, out


@pytest.fixture
def api_client():
    from rest_framework.test import APIClient
    return APIClient()


@pytest.mark.django_db
def test_login_wrong(client):
    url = reverse(log)
    data = {
       'user': 'wrong',
       'pass': 'wrong_password'
    }
    response = client.post(url, data=data)
    assert response.status_code == 200


@pytest.mark.django_db
def test_login_right(client):
    User.objects.create_user(username='right', password='right_password')
    url = reverse(log)
    data = {
       'user': 'right',
       'pass': 'right_password'
    }
    response = client.post(url, data=data)
    assert response.status_code == 302


@pytest.mark.django_db
def test_sign_right(client):
    url = reverse(sign)
    data = {
       'user': 'right',
       'pass': 'right_password'
    }
    response = client.post(url, data=data)
    assert response.status_code == 302


@pytest.mark.django_db
def test_add(client):
    url = '/api/'
    data = {
        "date": "2020-03-30",
        "name": "Парк Екатерингоф",
        "description": "Тихий уголок в шумном городе, пока ещё не тронутый реставрацией. Трава, пруд с лодками и лошади.",
        "coordinate_x": 59.902036,
        "coordinate_y": 30.260164,
        "image": "https://kudago.com/media/thumbs/s/images/place/cf/1d/cf1d428f2fecba8ef492b8b9f5bda7e0.jpg",
        "address": "ул. Лифляндская, д. 12",
        "type": "tree-deciduous"
    }
    response = client.post(url, data=data)
    assert response.status_code == 201


@pytest.mark.django_db
def test_view_8(client):
    url = '/api/'
    data = {
        "date": "2020-03-30",
        "name": "Парк Екатерингоф",
        "description": "Тихий уголок в шумном городе, пока ещё не тронутый реставрацией. Трава, пруд с лодками и лошади.",
        "coordinate_x": 59.902036,
        "coordinate_y": 30.260164,
        "image": "https://kudago.com/media/thumbs/s/images/place/cf/1d/cf1d428f2fecba8ef492b8b9f5bda7e0.jpg",
        "address": "ул. Лифляндская, д. 12",
        "type": "tree-deciduous"
    }
    client.post(url, data=data)
    url_1 = reverse(hello)
    data = {
       'data': '2020-03-30',
    }
    response = client.post(url_1, data=data)
    assert response.status_code == 200
