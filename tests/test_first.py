import pytest
from django.urls import reverse
from hello.views import hello, EventViewSet, log, sign, out

@pytest.mark.django_db
def test_view_1(client):
    url = ''
    response = client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_view_2(client):
    url = '/login/'
    response = client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_view_3(client):
    url = '/login/'
    response = client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_view_4(client):
    url = '/sign/'
    response = client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_view_5(client):
    url = '/api/'
    response = client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_view_6(client):
    url = '/api/0/'
    response = client.get(url)
    assert response.status_code == 404


@pytest.mark.django_db
def test_view_7(client):
    url = reverse(out)
    response = client.get(url)
    assert response.status_code == 302